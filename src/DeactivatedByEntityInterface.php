<?php
namespace BusinessDecision\Bundle\DoctrineFiltersBundle;

use Doctrine\ORM\Mapping\ClassMetadata;

interface DeactivatedByEntityInterface
{
    /**
     * @param string $class
     */
    public function disableForEntity(string $class): void;

    /**
     * @param string $class
     */
    public function enableForEntity(string $class): void;

    /**
     * @param ClassMetadata $targetEntity
     *
     * @return bool
     */
    public function isDisabled(ClassMetadata $targetEntity): bool;
}