<?php

namespace BusinessDecision\Bundle\DoctrineFiltersBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"METHOD","ANNOTATION"})
 */
final class DeactivatedFilter
{
    /**
     * @var string
     */
    public $filter;

    /**
     * @var string
     */
    public $entity;
}
