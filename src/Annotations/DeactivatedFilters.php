<?php

namespace BusinessDecision\Bundle\DoctrineFiltersBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
final class DeactivatedFilters
{
    /**
     * @var array<\BusinessDecision\Bundle\DoctrineFiltersBundle\Annotations\DeactivatedFilter>
     */
    public $value;
}
