<?php


namespace BusinessDecision\Bundle\DoctrineFiltersBundle;


use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use BusinessDecision\Bundle\DoctrineFiltersBundle\Annotations\DeactivatedFilter;
use BusinessDecision\Bundle\DoctrineFiltersBundle\Annotations\DeactivatedFilters;

class FilterDeactivatedConfiguratorSubscriber
{
    /**
     * ConfiguratorRightCheck constructor.
     *
     * @param EntityManager $em
     * @param Reader        $reader
     */
    public function __construct(EntityManagerInterface $em, Reader $reader)
    {
        $this->em = $em;
        $this->reader = $reader;
    }

    /**
     * @param FilterControllerEvent $event
     * @throws \ReflectionException
     */
    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();
        if(!is_array($controller)) {
            return;
        }
        $reflexion = new \ReflectionMethod($controller[0], $controller[1]);
        /** @var DeactivatedFilters $annotations */
        $annotations = $this->reader->getMethodAnnotation($reflexion,DeactivatedFilters::class);
        if ($annotations !== null) {
            foreach ($annotations->value as $annotation ) {
                $this->deactivateFilter($annotation);
            }
        }
        /** @var DeactivatedFilter $annotation */
        $annotation = $this->reader->getMethodAnnotation($reflexion,DeactivatedFilter::class);
        if ($annotation !== null) {
            $this->deactivateFilter($annotation);
        }
    }

    /**
     * @param DeactivatedFilter $annotation
     */
    private function deactivateFilter(DeactivatedFilter $annotation): void
    {
        $filterCollection = $this->em->getFilters();
        if($filterCollection->has($annotation->filter) && $filterCollection->isEnabled($annotation->filter)) {
            if(empty($annotation->entity)) {
                $this->deactivateFilterForEntity($annotation);
            } else {
                $filterCollection->disable($annotation->filter);
            }

        }

    }

    /**
     * @param DeactivatedFilter $annotation
     */
    private function deactivateFilterForEntity(DeactivatedFilter $annotation): void
    {
        $filterCollection = $this->em->getFilters();
        $filter = $filterCollection->getFilter($annotation->filter);
        if ($filter instanceof DeactivatedByEntityInterface) {
            $filter->disableForEntity($annotation->entity);
        }
    }
}