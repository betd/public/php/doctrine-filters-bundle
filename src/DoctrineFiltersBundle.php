<?php

namespace BusinessDecision\Bundle\DoctrineFiltersBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class QuerySecurityBundle.
 */
class DoctrineFiltersBundle extends Bundle
{
}
