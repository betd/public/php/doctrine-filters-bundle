<?php


namespace BusinessDecision\Bundle\DoctrineFiltersBundle;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

interface FilterConfiguratorInterface
{
    public function onKernelRequest(GetResponseEvent $event): void;
}