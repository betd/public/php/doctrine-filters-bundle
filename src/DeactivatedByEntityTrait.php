<?php

namespace BusinessDecision\Bundle\DoctrineFiltersBundle;

use Doctrine\ORM\Mapping\ClassMetadata;

trait DeactivatedByEntityTrait
{
    protected $disabled = [];
    /**
     * @param string $class
     */
    public function disableForEntity(string $class): void
    {
        $this->disabled[$class] = true;
    }
    /**
     * @param string $class
     */
    public function enableForEntity(string $class): void
    {
        $this->disabled[$class] = false;
    }


    /**
     * @param ClassMetadata $targetEntity
     *
     * @return bool
     */
    public function isDisabled(ClassMetadata $targetEntity): bool
    {
        $class = $targetEntity->getName();
        if (array_key_exists($class, $this->disabled) && $this->disabled[$class] === true) {
            return true;
        } elseif (array_key_exists($targetEntity->rootEntityName, $this->disabled) && $this->disabled[$targetEntity->rootEntityName] === true) {
            return true;

        }

        return false;
    }

}