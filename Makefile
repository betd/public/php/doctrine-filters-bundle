ifndef VERBOSE
.SILENT:
endif
EXIT_CODE = 0
ifeq ($(origin CI), undefined)
EXIT_CODE = 1
endif
args = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

LOCAL_APP_PATH = ./
IMAGE_AUDIT = mykiwi/phaudit:7.2
QA        = docker run --rm -v `pwd`/$(LOCAL_APP_PATH):/project $(IMAGE_AUDIT)
COMPOSER =  docker run --rm --interactive --tty -v `pwd`/$(LOCAL_APP_PATH):/app --user $(id -u):$(id -g) composer

apply-php-cs-fixer: ## apply php-cs-fixer fixes
	docker pull $(IMAGE_AUDIT)
	$(QA) php-cs-fixer fix --using-cache=no --verbose --diff

phpstan: ## PHP Static Analysis Tool (https://github.com/phpstan/phpstan)
	$(QA) phpstan analyse -l 0 -c .phpstan.neon --memory-limit=512M src

composer:
	$(COMPOSER) $(call args,install)

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help apply-php-cs-fixer phpstan composer connect-qa

%:
	@:
